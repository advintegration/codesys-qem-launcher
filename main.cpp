#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
    int r = setuid(0);
    if (!r) {
        return system("chroot /opt/codesys/ /bin/codesyscontrol.bin /etc/CODESYSControl.cfg");
    }
    return r;
}
